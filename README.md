# Qwertone

Simple music synthesizer app (like a toy-piano), but based on usual qwerty-keyboard for input.

[![screenshot.png](https://i.postimg.cc/hjTJqy82/screenshot.png)](https://postimg.cc/yg66mTbR)

## Install

For now, packages are available only for Linux, and only the following options:

* [Flatpak package](https://flathub.org/apps/details/com.gitlab.azymohliad.Qwertone) for any Linux distribution
* [AUR package](https://aur.archlinux.org/packages/qwertone-git/) for ArchLinux and derivatives

If it doesn't cover your platform, you can build it yourself.

## Build and run

1. [Install Rust](https://www.rust-lang.org/tools/install)
2. Install GTK
3. Build
```
cargo build --release
```
4. Run
```
cargo run --release
```